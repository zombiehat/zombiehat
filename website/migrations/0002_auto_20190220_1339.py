# Generated by Django 2.1.7 on 2019-02-20 13:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CourseSubChapter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('markdown_text', models.TextField()),
            ],
        ),
        migrations.AlterField(
            model_name='coursechapter',
            name='course',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='course', to='website.Course'),
        ),
        migrations.AddField(
            model_name='coursesubchapter',
            name='course_chapter',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='course_chapter', to='website.CourseChapter'),
        ),
    ]
