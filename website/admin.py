from django.contrib import admin
from .models import Profile, Course, CourseChapter, CourseCompleted, CourseChapterCompleted, CourseSummary, CourseStart, LogVal
from django.contrib.auth.models import User

class ChapterInline(admin.StackedInline):
    model = CourseChapter
    exclude=("total_completed ",)
    readonly_fields=('total_completed', )

class CourseAdmin(admin.ModelAdmin):
    inlines = [
        ChapterInline,
    ]
    exclude=("total_completed ",)
    readonly_fields=('total_completed', )

class CourseSummaryAdmin(admin.ModelAdmin):
    model = CourseSummary
    change_list_template = 'website/change_list.html'
    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
                    request,
                    extra_context=extra_context,
                )
        courses = Course.objects.all()
        response.context_data['courses'] = []
        response.context_data['completed'] = []
        response.context_data['not_completed'] = []
        course_total_count = {}
        course_completed_count = {}
        course_not_completed_count = {}
        for course in courses:
            response.context_data['courses'].append(course.name)
            # course_total_count[course] = CourseChapter.objects.filter(course=course).count()
            course_completed_count[course] = CourseCompleted.objects.filter(course=course).count()
            course_not_completed_count[course] = User.objects.filter(groups__name=course.group_for).count() - course_completed_count[course]
            response.context_data['completed'].append(course_completed_count[course])
            response.context_data['not_completed'].append(course_not_completed_count[course])
        return response

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

# Register your models here.
admin.site.register(Profile)
admin.site.register(Course, CourseAdmin)
admin.site.register(CourseSummary, CourseSummaryAdmin)
# admin.site.register(CourseChapter)
# admin.site.register(CourseStart)
# admin.site.register(CourseCompleted)
# admin.site.register(CourseChapterCompleted)
# admin.site.register(LogVal)

admin.site.site_header = "Shantha Sanofi"
admin.site.site_title = "Shantha Sanofi Portal"
admin.site.index_title = "Welcome to Shantha Sanofi Portal"