from django.db import models
from cloudinary.models import CloudinaryField
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

# Create your models here.


# DEPARTMENT_CHOICES = (
#         ('Industrial Performance', 'Industrial Performance'),
#         ('Clinical Research Department', 'Clinical Research Department'),
#         ('Site Services', 'Site Services'),
#         ('Quality Assurance', 'Quality Assurance'),
#         ('Corporate Site Services', 'Corporate Site Services'),)

# class Department(models.Model):
#     name = models.CharField(max_length=100)


class Profile(models.Model):
    user = models.OneToOneField(
        User, unique=True, null=True, on_delete=models.CASCADE, related_name='profile')
    designation = models.CharField(max_length=100, blank=True)
    # courses_completed = models.IntegerField()
    # joining_date = models.DateField()
    # https://docs.djangoproject.com/en/2.1/ref/contrib/auth/ lists all fields in Django's user model.
    # Profile class is an extension of that class
    # department = models.ManyToManyField(Department)
    # Form only will allow one department, many to many just in case they ask
    # to change

    def __str__(self):
        return self.user.username

# Every Course has Chapters.


class Course(models.Model):
    # added_by = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='added_by')
    name = models.CharField(max_length=100)
    # deadline = models.DateTimeField(null=True)
    # total_chapter = models.IntegerField(default=0)
    no_of_days = models.IntegerField(default=10)
    total_completed = models.IntegerField(default=0)
    difficulty = models.CharField(default='Medium', max_length=100)
    group_for = models.ForeignKey(Group, related_name='coursegroup', null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class CourseChapter(models.Model):
    course = models.ForeignKey(
        Course, on_delete=models.CASCADE, related_name='course')
    name = models.CharField(max_length=100)
    markdown_text = models.TextField(blank=True)
    total_completed = models.IntegerField(default=0)

    def __str__(self):
        return self.name

# class CourseSubChapter(models.Model):
#     course_chapter = models.ForeignKey(CourseChapter, on_delete=models.CASCADE, related_name='course_chapter')
#     name = models.CharField(max_length=100)
     # Text will be stored in markdown for easy formatting
    # video_id = models.CharField(max_length=100)
    # Images will be part of markdown, videos not sure if possible

    # def __str__(self):
    #     return self.name


class CourseCompleted(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='UserCourseCompleted')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='CourseCompleted')
    timestamp = models.DateTimeField(null=True)

class CourseChapterCompleted(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='UserCourseChapterCompleted')
    chapter = models.ForeignKey(CourseChapter, on_delete=models.CASCADE, related_name='CourseChapterCompleted')
    timestamp = models.DateTimeField(null=True)

class CourseStart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='UserCourseStart')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='CourseStart')
    timestamp = models.DateTimeField(null=True)

class CourseSummary(CourseCompleted):
    class Meta:
        proxy = True
        verbose_name = 'Course Summary'
        verbose_name_plural = 'Courses Summary'

class LogVal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='UserLogVal')
    val = models.CharField(max_length=100)
# class Photo(models.Model):
#     image = CloudinaryField('image')
