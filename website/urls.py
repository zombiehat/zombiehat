from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('profile', views.profile, name='profile'),
    path('course/<int:course_id>/', views.course, name='course'),
    path('course_list/', views.course_list, name='course_list'),
    path('chapter_complete/', views.chapter_complete, name='chapter_complete'),
    path('quiz/', views.quiz, name='quiz'),
    path('logresult/', views.logresult, name='logresult'),
    path('login/', auth_views.LoginView.as_view(template_name='website/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),
    # path('upload_prompt/', views.upload_prompt, name='upload_prompt'),
    # path('direct_upload_complete/', views.direct_upload_complete, name='direct_upload_complete'),
]
