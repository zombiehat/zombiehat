from django.shortcuts import render, redirect, get_object_or_404

from django import forms
from django.http import HttpResponse, JsonResponse
from .models import Profile, Course, CourseChapter, CourseCompleted, CourseChapterCompleted, CourseStart, LogVal
from django.contrib.auth.decorators import user_passes_test, login_required
from datetime import datetime, timedelta, timezone
# from cloudinary.forms import cl_init_js_callbacks
# from .models import Photo
# from .forms import PhotoDirectForm

# import json
# from django.views.decorators.csrf import csrf_exempt


# def upload_prompt(request):
#     context = dict(direct_form=PhotoDirectForm())
#     cl_init_js_callbacks(context['direct_form'], request)
#     return render(request, 'media.html', context)


# @csrf_exempt
# def direct_upload_complete(request):
#     form = PhotoDirectForm(request.POST)
#     if form.is_valid():
#         form.save()
#         ret = dict(photo_id=form.instance.id)
#     else:
#         ret = dict(errors=form.errors)

#     return HttpResponse(json.dumps(ret), content_type='application/json')

def profile_details(request):
    details = {}
    if request.user.first_name:
        details['name'] = request.user.first_name
    if request.user.last_name:
        details['name'] += (' ' + request.user.last_name)
    if request.user.email:
        details['email'] = request.user.email
    if request.user.profile.designation:
        details['designation'] = request.user.profile.designation
    return details


def progress_details(request):
    courses = []
    groups = request.user.groups.all()
    for group in groups:
        group_courses = Course.objects.filter(group_for=group)
        courses.extend(group_courses)
    course_details = []
    for course in courses:
        count = CourseChapterCompleted.objects.filter(
            chapter__course=course,user=request.user).count()
        total_count = CourseChapter.objects.filter(course=course).count()
        entry = {course.name: {'completed_chapter': count,
                               'total_chapter': total_count}}
        course_details.append(entry)
    return course_details


def index(request):
    return render(request, 'website/index.html')


@user_passes_test(lambda u: not u.is_authenticated, redirect_field_name=None, login_url='/')
def login(request):
    return render(request, 'website/login.html')


@login_required
def profile(request):
    details = profile_details(request)
    progress = progress_details(request)
    context = {
        'details': details,
        'progress': progress
    }
    return render(request, 'website/profile.html', context)

@login_required
def course_list(request):
    courses_start = []
    courses_notstart = []
    courses_completed = []
    groups = request.user.groups.all()
    completed_course_list = []
    completed_courses = CourseCompleted.objects.filter(user=request.user)
    for completed_course in completed_courses:
        completed_course_list.append(completed_course.course)
    started_course_list = []
    started_courses = CourseStart.objects.filter(user=request.user)
    for started_course in started_courses:
        started_course_list.append(started_course.course)
    for group in groups:
        group_courses = Course.objects.filter(group_for=group)
        for gc in group_courses:
            if gc in completed_course_list:
                courses_completed.append(gc)
            elif gc in started_course_list:
                courses_start.append(gc)
            else:
                courses_notstart.append(gc)
    percentage = (len(courses_completed)/(len(courses_completed) + len(courses_notstart) + len(courses_start))) * 100
    recommended_courses = []
    if LogVal.objects.filter(user=request.user).exists():
        lv = LogVal.objects.get(user=request.user)
        for course in courses_notstart:
            if course.difficulty == lv.val:
                recommended_courses.append(course)
    context = {'al': courses_start,'cl':courses_notstart, 'bl': courses_completed, 'percentage': percentage, 'recommended_courses': recommended_courses}
    return render(request, 'website/tasks.html', context)

@login_required
def course(request, course_id):
    course = get_object_or_404(Course,pk=course_id)
    is_complete = CourseCompleted.objects.filter(course=course,user=request.user).exists()
    if is_complete:
        return redirect('index')
    if not CourseStart.objects.filter(course=course,user=request.user).exists():
        new_start = CourseStart()
        new_start.user = request.user
        new_start.course = course
        new_start.timestamp = datetime.now()
        new_start.save()
    starttime = CourseStart.objects.get(course=course,user=request.user).timestamp
    time_left =  starttime + timedelta(days=10) - datetime.now(timezone.utc)
    course_chapters = CourseChapter.objects.filter(course=course)
    chapter_list = {}
    chapter_complete = {}
    context = {}
    for chapter in course_chapters:
        chapter_list[chapter.name] = {'text': chapter.markdown_text, 'exists': CourseChapterCompleted.objects.filter(chapter=chapter,user=request.user).exists()}
    context['name'] = course.name
    context['id'] = course.id
    context['chapters'] = chapter_list
    context['time_left'] = time_left
    context['deadline'] = starttime + timedelta(days=10)
    return render(request, 'website/starttask.html', context)

@login_required
def chapter_complete(request):
    user = request.user
    chapter_name = request.GET.get('chapter', None)
    chapter = CourseChapter.objects.get(name=chapter_name)
    chapter_count = CourseChapter.objects.filter(course=chapter.course).count()
    complete_chapter = CourseChapterCompleted()
    complete_chapter.user = user
    complete_chapter.chapter = chapter
    complete_chapter.timestamp = datetime.now()
    complete_chapter.save()
    chapter.total_completed += 1
    chapter.save()
    complete_count = CourseChapterCompleted.objects.filter(user=user, chapter__course=chapter.course).count()
    print(chapter_count)
    print(complete_count)
    if chapter_count <= complete_count:
        course = chapter.course
        complete_course = CourseCompleted()
        complete_course.user = user
        complete_course.course = course
        complete_course.timestamp = datetime.now()
        complete_course.save()
        course.total_completed += 1
        course.save()
    return JsonResponse({'completed_chapter': True})

@login_required
def quiz(request):
    return render(request, 'website/quiz.html')

@login_required
def logresult(request):
    user = request.user
    logval = request.GET.get('logval', None)
    diff_dict = {'0':'Hard', '1': 'Medium', '2': 'Easy', None: 'Easy'}
    if not LogVal.objects.filter(user=request.user).exists():
        lv = LogVal()
        lv.user = request.user
        lv.val = diff_dict[logval]
        lv.save()
    else:
        lv = LogVal.objects.get(user=request.user)
        lv.val = diff_dict[logval]
        lv.save()
    return JsonResponse({'completed_chapter': True})